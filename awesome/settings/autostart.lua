local awful = require("awful")

-- {{{ Autostart
awful.spawn.with_shell("xrandr --output Virtual-1 --mode 1920x1080")
awful.spawn.with_shell("nitrogen --restore")
awful.spawn.with_shell("picom")
-- }}}
