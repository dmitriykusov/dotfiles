-- set options
require "core/options"

-- download and install plugins
require "bootstrap"

-- plugins configs
require "configs/colorscheme"
require "configs/tmux"
require "configs/maximize"
require "configs/tree"
require "configs/lualine"
require "configs/bufferline"
require "configs/telescope"
require "configs/treesitter"
require "configs/gitsigns"
require "configs/dressing"
require "configs/toggleterm"

-- set mappings
require "core/mappings"
