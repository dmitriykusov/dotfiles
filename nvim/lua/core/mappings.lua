vim.g.mapleader = ","
local keymap = vim.keymap

-- split windows
keymap.set("n", "<C-v>", "<C-w>v") -- Split window vertically
keymap.set("n", "<C-s>", "<C-w>s") -- Split window horizontally
-- maximize/unmaximize
keymap.set("n", "<C-m>", "<Cmd>lua require('maximize').toggle()<CR>")

-- tabs
keymap.set("n", "<C-t>", ":tabnew<CR>") -- Create new tab
keymap.set("n", "<C-w>", ":tabclose<CR>") -- Close current tab
-- navigate between tabs
keymap.set("n", "<C-n>", ":tabnext<CR>") -- Next buffer
keymap.set("n", "<C-p>", ":tabprev<CR>") -- Prev buffer

-- enter normal mode
keymap.set("i", "jk", "<ESC>")
keymap.set("i", "kj", "<ESC>")

-- toggle file explorer
keymap.set("n", "<C-e>", ":NvimTreeToggle<CR>")

-- telescope shortcuts
local builtin = require('telescope.builtin')
keymap.set('n', '<leader>ff', builtin.find_files, {})
keymap.set('n', '<leader>fg', builtin.live_grep, {})
keymap.set('n', '<leader>fb', builtin.buffers, {})
keymap.set('n', '<leader>fh', builtin.help_tags, {})

-- terminal
keymap.set('n', '<C-x>', ":ToggleTerm<CR>")
