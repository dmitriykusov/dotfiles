vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost bootstrap.lua source <afile> | PackerSync
  augroup end
]])

local ensure_packer = function()
  local fn = vim.fn
  local install_path = fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
  if fn.empty(fn.glob(install_path)) > 0 then
    fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
    vim.cmd [[packadd packer.nvim]]
    return true
  end
  return false
end

local packer_bootstrap = ensure_packer()

return require('packer').startup(function(use)
  -- packer itself
  use 'wbthomason/packer.nvim'

  -- tmux integration
  use 'https://github.com/aserowy/tmux.nvim'
  -- maximize/unmaximize plugin
  use 'https://github.com/declancm/maximize.nvim'
  -- file explorer
  use 'https://github.com/nvim-tree/nvim-web-devicons'
  use 'https://github.com/nvim-tree/nvim-tree.lua'
  -- statusline
  use 'https://github.com/nvim-lualine/lualine.nvim'
  -- tabline
  use 'https://github.com/akinsho/bufferline.nvim'
  -- catpuccin colorscheme
  use 'https://github.com/catppuccin/nvim'
  -- fuzzy finder
  use {'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.0',
    requires = { {'nvim-lua/plenary.nvim'} }
  }
  -- treesitter
  use 'https://github.com/nvim-treesitter/nvim-treesitter'
  -- git decorations
  use 'https://github.com/lewis6991/gitsigns.nvim'
  -- UI improvements
  use 'https://github.com/stevearc/dressing.nvim'
  -- Built-in terminal
  use 'https://github.com/akinsho/toggleterm.nvim'

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if packer_bootstrap then
    require('packer').sync()
  end
end)

